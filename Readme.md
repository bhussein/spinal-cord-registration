# Spinal Cord Registration

A tool to register two spinal cord images.

It registers rigidly and non-linearly two spinal cord images from a given patient. The method is designed to be robust to difference of sagital coverage and to partial overlap in the two images. However, images must have similar intensity characteristics (typically same sequence).


## How does it work?

Spinal cord images can be in very different spaces (with different origins and orientations) from one time point to another. Moreover, two time points can only partially overlap. 
A standard rigid registration is likely to fail in such hard cases. 

The approach used to register the time points is to split both images in patches (along the spinal cord), register the obtained pair of patches, and take the best transform (which gives the best image alignment according to a custom similarity metric).
The similarity metric is the number of SIFT and ORB feature points ([computed with OpenCV](https://docs.opencv.org/3.4/db/d27/tutorial_py_table_of_contents_feature2d.html)) in the 2D center sagittal slice, which match in both images (two feature points match when they have the same position in both images).

Finally, since we would prefer the images to be perfectly aligned in the center (instead of perfectly aligned at the top, and poorly at the bottom) we try a last registration with centered patches, initialized with the previous best transform. If this last registration gives a better image alignment, we keep the obtained transformation, otherwise we ignore it.

For a detailed description of how the algoritm works please refer to this [note](https://notes.inria.fr/cLhmaUkuR0q4y9axZz0byw).

Here are a few example of feature matching:

- SIFT feature matches on the resulting images (resampled fixed image, resampled and registered moving image), the registration is computed from the patch 0 of fixed image and patch 0 of the moving image:
   ![SIFT features matched on the patches 0 and 0](images/patch_0_on_0_sift_all.png)

- Same as above, but computed from the patch 0 of fixed image and patch 1 of the moving image:
   ![SIFT features matched on the patches 0 and 1](images/patch_0_on_1_sift_all.png)

- Same as above, but computed from the center patch of fixed image and the center patch of the moving image:
   ![SIFT features matched on the patches 1 and 1](images/final_registration_sift_all.png)

Once the rigid registration is computed, a non linear registration is computed with anima or elastix, or both. Anima will compute a dense deformation field, elastix will compute a bspline transformation. 

## Clone this repository
Assume you have git installed and permission to the repo: 

- `git clone https://gitlab.inria.fr/amasson/spinal-cord-registration.git`
- `cd spinal-cord-registration/`

## Create a new Python enviroment and Install Dependencies
Create a new pyhon enviroment where you will install the required dependencies. Follow the following steps:


- `python3 -m venv /path/to/the/new/enviroment/myvenv_env`

- `source myvenv_env/bin/activate`

- `pip install -r requirements.txt`

### Install SCT and Anima

- Along with the requirement.txt, you also need to install sct toolbox and Anima: You can follow these steps to install both dependecies:

```
# Installing the Spinal Cord Toolbox
- git clone --single-branch -b master --depth 1 https://github.com/spinalcordtoolbox/spinalcordtoolbox.git
- cd spinalcordtoolbox
- pip install --upgrade pip
- ./install_sct -y -c

# You can add the path of the sct to you bashrc file so it could be accessed in the terminal with the editor of your choice, here we use vim:
- vim ~/.bashrc
# then past the path of the sct example below:
- PATH="$PATH:/path/to/spinalcordtoolbox/bin"
# save the changes and run:
- source ~/.bashrc

# Install Anima

- wget https://github.com/Inria-Empenn/Anima-Public/releases/download/v4.2/Anima-Ubuntu-4.2.zip
- unzip Anima-Ubuntu-4.2.zip
- ls -la Anima-Binaries-4.2
- rm Anima-Ubuntu-4.2.zip
- git clone --single-branch -b master --depth 1 https://github.com/Inria-Empenn/Anima-Scripts-Public.git 
- cd Anima-Scripts-Public
- python configure.py -a /Anima/Anima-Binaries-4.2/ -s /Anima/Anima-Scripts-Public/

# Similar to the sct, you can add Anima path to the bashrc file so it will accessible to the terminal, again we use vim as a example:
- vim ~/.bashrc
# then past the path of the sct example below:
- ENV PATH="/path/to/Anima/Anima-Binaries-4.2:$PATH"
# save the changes and run:
- source ~/.bashrc

```

## Usage

`python register.py -f path/to/fu.nii.gz -m path/to/bl.nii.gz -o path/to/output/folder -kif`, `-f` and `-m` being the fixed and moving images respectively.

If running with the `keep_intermediate_files (-kif)`, all the intermediate files will be saved in the temporary folder within the output folder. Here is an example of some of the generated files:
```
output_dir
   ├── bl_registered_nl_matches_fu.nii.gz
   ├── bl_registered_nl.nii.gz
   └── output_dir_tmp
      ├── bl_moving.nii.gz 
      ├── bl_moving_reoriented.nii.gz
      ├── bl_moving_resampled.nii.gz
      ├── bl_registered_centerline.csv
      ├── bl_registered_centerline.nii.gz
      ├── bl_registered_moving_masked.nii.gz
      ├── bl_registered.nii.gz
      ├── bl_registered_nl_matches_fu.nii.gz
      ├── bl_registered_nl.nii.gz
      ├── fu_fixed.nii.gz 
      ├── fu_fixed_reoriented.nii.gz
      ├── fu_fixed_resampled_centerline.csv
      ├── fu_fixed_resampled_centerline.nii.gz
      ├── fu_fixed_resampled_fixed_masked.nii.gz
      ├── fu_fixed_resampled.nii.gz
      ├── fu_fixed_resampled.png
      ├── mask_bl_registered.nii.gz
      ├── mask_fu_fixed_resampled.nii.gz
      ├── mask_union.nii.gz
      ├── similarity.txt
      ├── transform.nii.gz
      └── transform.txt
```
 Othwise if running without keeping the intermediate files:
- `python register.py -f path/to/fu.nii.gz -m path/to/bl.nii.gz -o path/to/output/folder`

You should end-up with the two registed images:

 ```
 output_dir
   ├── bl_registered_nl_matches_fu.nii.gz
   └── bl_registered_nl.nii.gz
 ```
Some of the output directories:
 - `fixedName_fixed_reoriented.nii.gz`: the fixed image reoriented,
 - `movingName_moving_reoriented.nii.gz,` the moving image reoriented,
 - `fixedName_resampled.nii.gz`: the fixed image resampled (so that both images are in the same space),
 - `movingName_resampled.nii.gz`: the moving image resampled (so that both images are in the same space),
 - `movingName_registered.nii.gz`: the moving image registered rigidly,
 - `movingName_registered_nl.nii.gz`: the moving image registered rigidly and non linearly,
 - `movingName_registered_nl_matches_fixedName.nii.gz`: the moving image registered rigidly and non linearly with similar image proprties as the fixed image,
 - `transform.nii.gz`: the rigid transform to go from the fixed image to the moving image,
 - `transform_nl.nii.gz`: the non-linear transform,
 - `similarity.txt`: the similarity measures.

Output file names can be modified with the corresponding arguments (`--fixed_resampled` enables to set the name of the resampled fixed image, etc.).

Use `python register.py --help` to get more help.

### Batch usage

The images must be structured in the following manner:
```
patients_to_register/
├── patient01
│   ├── 2017-04-19
│   │   ├── 0.nii.gz
│   │   ├── 1.nii.gz
│   │   ├── 2.nii.gz
│   │   └── combined.nii.gz
│   └── 2018-05-03
│   │   ├── 0.nii.gz
│   │   ├── 1.nii.gz
│   │   ├── 3.nii.gz
│   │   └── combined.nii.gz
│   └── 2019-05-03
│       ├── 0.nii.gz
│       ├── 1.nii.gz
│       ├── 2.nii.gz
│       ├── 3.nii.gz
│       └── combined.nii.gz
├── patient02
│...
```

The first time point will be registered on the newer ones.
Images with the same name in different time points will be registered together.

In the example above :
```
patient01/2017-04-19/0.nii.gz will be registered on patient01/2018-05-03/0.nii.gz
patient01/2017-04-19/0.nii.gz will be registered on patient01/2019-05-03/0.nii.gz
patient01/2017-04-19/1.nii.gz will be registered on patient01/2018-05-03/1.nii.gz
patient01/2017-04-19/1.nii.gz will be registered on patient01/2019-05-03/1.nii.gz
patient01/2017-04-19/2.nii.gz will be registered on patient01/2019-05-03/2.nii.gz
patient01/2018-05-03/3.nii.gz will be registered on patient01/2019-05-03/3.nii.gz
```
The similarity measures will be recorded in the `similarities.csv` file.

The output file names will follow the same patterns as when processing a single patient.

### Notes

The tool can be configured with the configuration file (`config.yml` by default) and the following arguments:

```
  -kif, --keep_intermediate_files
                        Keep intermediate files. (default: False)
  -sr, --skip_rigid_if_exists
                        Skip rigid registration if output files already exist. (default: False)
  -cp, --create_previews
                        Create image previews. (default: False)
  -cfg CONFIGURATION, --configuration CONFIGURATION
                        Path to the configuration file. (default: config.yml)
  -dbg, --debug         Debug will output previews of the feature matches. (default: False)
```

## Docker Image
To build docker image, simply run the following command:

- `docker build -t image_name .`

After building the image, you can use the following command to run registration using the docker image

- `docker run -v /path/to/fixed_image/fu.nii.gz:/data/input/fu.nii.gz -v /path/to/moving_image/bl.nii.gz:/data/input/bl.nii.gz image_name:image_tag`

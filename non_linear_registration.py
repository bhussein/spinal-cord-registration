import shutil
import tempfile

import SimpleITK as sitk

import utils
from anima_utils import *


def register_anima_non_linear(fixed_path, moving_path, output_path, output_transform_path, args=None):
	args = args or utils.configuration['default_non_linear_registration_args']
	pyramid_options = get_registration_options_from_image(fixed_path)
	try:
		call([animaDenseSVFBMRegistration, '-r', fixed_path, '-m', moving_path, '-o', output_path, '-O', output_transform_path] + args + pyramid_options)
	except Exception as e:
		if len(e.args) == 2 and 'Command exited with status' in e.args[0] and e.args[1] != 0:
			pyramid_options = pyramid_options[:-1] + [str(int(pyramid_options[-1])-1)]
			call([animaDenseSVFBMRegistration, '-r', fixed_path, '-m', moving_path, '-o', output_path, '-O', output_transform_path] + args + pyramid_options)

	return output_path, output_transform_path

def bspline_intra_modal_registration(
	fixed_image,
	moving_image,
	fixed_image_mask=None,
	# fixed_points=None,
	# moving_points=None,
):

	registration_method = sitk.ImageRegistrationMethod()

	# Determine the number of BSpline control points using the physical spacing we want for the control grid.
	grid_physical_spacing = [50.0, 50.0, 50.0]  # A control point every 50mm
	image_physical_size = [
		size * spacing
		for size, spacing in zip(fixed_image.GetSize(), fixed_image.GetSpacing())
	]
	mesh_size = [
		int(image_size / grid_spacing + 0.5)
		for image_size, grid_spacing in zip(image_physical_size, grid_physical_spacing)
	]

	initial_transform = sitk.BSplineTransformInitializer(
		image1=fixed_image, transformDomainMeshSize=mesh_size, order=3
	)
	registration_method.SetInitialTransform(initial_transform)

	registration_method.SetMetricAsMeanSquares()
	# Settings for metric sampling, usage of a mask is optional. When given a mask the sample points will be
	# generated inside that region. Also, this implicitly speeds things up as the mask is smaller than the
	# whole image.
	registration_method.SetMetricSamplingStrategy(registration_method.REGULAR)
	registration_method.SetMetricSamplingPercentage(0.5)
	if fixed_image_mask:
		registration_method.SetMetricFixedMask(fixed_image_mask)

	# Multi-resolution framework.
	registration_method.SetShrinkFactorsPerLevel(shrinkFactors=[4, 2, 1])
	registration_method.SetSmoothingSigmasPerLevel(smoothingSigmas=[2, 1, 0])
	registration_method.SmoothingSigmasAreSpecifiedInPhysicalUnitsOn()

	registration_method.SetInterpolator(sitk.sitkLinear)
	registration_method.SetOptimizerAsLBFGSB(
		gradientConvergenceTolerance=1e-5, numberOfIterations=100
	)

	# If corresponding points in the fixed and moving image are given then we display the similarity metric
	# and the TRE during the registration.
	# if fixed_points and moving_points:
	#     registration_method.AddCommand(
	#         sitk.sitkStartEvent, rc.metric_and_reference_start_plot
	#     )
	#     registration_method.AddCommand(
	#         sitk.sitkEndEvent, rc.metric_and_reference_end_plot
	#     )
	#     registration_method.AddCommand(
	#         sitk.sitkIterationEvent,
	#         lambda: rc.metric_and_reference_plot_values(
	#             registration_method, fixed_points, moving_points
	#         ),
	#     )

	return registration_method.Execute(fixed_image, moving_image)

def register_sitk_non_linear(fixed_path, moving_path, output_path, output_transform_path, args=None):
	fixed_image = sitk.ReadImage(str(fixed_path), sitk.sitkFloat64)
	moving_image = sitk.ReadImage(str(moving_path), sitk.sitkFloat64)
	final_transform = bspline_intra_modal_registration(fixed_image, moving_image)
	registered = utils.apply_transform(fixed_image, moving_image, final_transform)
	sitk.WriteImage(registered, str(output_path))
	sitk.WriteTransform(final_transform, str(output_transform_path))
	return output_path, output_transform_path

def get_spinal_cord_mask(image_path, keep_intermediate_files=False):
	##### Process T2* image - crop around spinal cord & convert to isotropic dimensions. ###########
	# Detect the centerline of the spinal cord.
	call(['sct_get_centerline', '-i', image_path, '-c', 't2'])
	centerline_path = image_path.parent / image_path.name.replace('.nii.gz', '_centerline.nii.gz')
	mask_path = image_path.parent / f'mask_{image_path.name}'

	# Create a mask around the previously detected centerline. 
	# Set size slightly bigger than our eventual desired patch size to make sure we capture the relevant area in both T2s and later in T2.
	call(['sct_create_mask', '-size', '30mm', '-i', image_path, '-p', f'centerline,{centerline_path}', '-o', mask_path])

	if not keep_intermediate_files:
		centerline_path.unlink()
		centerline_csv_path = image_path.parent / image_path.name.replace('.nii.gz', '_centerline.csv')
		if centerline_csv_path.exists():
			centerline_csv_path.unlink()
	return mask_path

def sct_crop_image(image_path, mask_path, output_path):
	call(['sct_crop_image', '-i', image_path, '-m', mask_path, '-o', output_path])
	return output_path

def mask_from_spinal_cord(fixed_path, moving_path, output_folder, keep_intermediate_files=False):
	fixed_mask_path = get_spinal_cord_mask(fixed_path, keep_intermediate_files)
	moving_mask_path = get_spinal_cord_mask(moving_path, keep_intermediate_files)
	mask_union_path = utils.get_masks_union(fixed_mask_path, moving_mask_path, output_folder / 'mask_union.nii.gz')
	fixed_masked_path = sct_crop_image(fixed_path, mask_union_path, output_folder / f'{fixed_path.name[:-7]}_fixed_masked.nii.gz')
	moving_masked_path = sct_crop_image(moving_path, mask_union_path, output_folder / f'{moving_path.name[:-7]}_moving_masked.nii.gz')
	if not keep_intermediate_files:
		fixed_mask_path.unlink()
		moving_mask_path.unlink()
		mask_union_path.unlink()
	return fixed_masked_path, moving_masked_path

def register_non_linear_from_paths(fixed_path, moving_path, moving_registered_path, moving_registered_nl_path, transform_path, transform_nl_path, output_folder, similarity_path, keep_intermediate_files=False):
	fixed_masked_path, moving_registered_masked_path = mask_from_spinal_cord(fixed_path, moving_registered_path, output_folder, keep_intermediate_files)
	utils.mask_images_intersection(fixed_masked_path, moving_registered_masked_path)
	register_anima_non_linear(fixed_masked_path, moving_registered_masked_path, moving_registered_nl_path, transform_nl_path)
	# transform_nl_path = transform_nl_path.parent / (transform_nl_path.name[:-7] + '.tfm') if transform_nl_path.name.endswith('.nii.gz') else transform_nl_path
	# register_sitk_non_linear(fixed_masked_path, moving_registered_masked_path, moving_registered_nl_path, transform_nl_path)
	utils.apply_transforms_sitk(fixed_path, moving_path, [transform_path, transform_nl_path], moving_registered_nl_path)
	similarity = utils.compute_similarity_from_path(fixed_path, moving_registered_nl_path)
	utils.edit_json(similarity_path, 'non_linear_similarity', similarity)
	return


def anima_to_elastix_rigid_transform(anima_transform_path, image_path, elastix_transform_path):
	transform = sitk.ReadTransform(str(anima_transform_path))
	image = sitk.ReadImage(str(image_path))
	etr = sitk.Euler3DTransform()
	etr.SetMatrix(transform.GetMatrix())

	with open('TransformParameters.0.txt', 'r') as f:
		content = f.read()
		dir = image.GetDirection()
		direction = [dir[0], dir[3], dir[6], dir[1], dir[4], dir[7], dir[2], dir[5], dir[8]]
		direction = ' '.join([str(d) for d in direction])
		content = content.replace('(Direction 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0)', f'(Direction {direction})')
		origin = ' '.join([str(c) for c in image.GetOrigin()])
		content = content.replace('(Origin 0.0 0.0 0.0)', f'(Origin {origin})')
		size = ' '.join([str(c) for c in image.GetSize()])
		content = content.replace('(Size 0 0 0)', f'(Size {size})')
		spacing = ' '.join([str(c) for c in image.GetSpacing()])
		content = content.replace('(Spacing 0.0 0.0 0.0)', f'(Spacing {spacing})')
		translation = transform.GetTranslation()
		transform_parameters = [etr.GetAngleX(), etr.GetAngleY(), etr.GetAngleZ(), translation[0], translation[1], translation[2]]
		transform_parameters = ' '.join([str(c) for c in transform_parameters])
		content = content.replace('(TransformParameters 0.0 0.0 0.0 0.0 0.0 0.0)', f'(TransformParameters {transform_parameters})')
		with open(elastix_transform_path, 'w') as f:
			f.write(content)
	return elastix_transform_path
	

def register_non_linear_elastix_from_paths(fixed_resampled_path, moving_resampled_path, moving_registered_path, moving_registered_nl_path, transform_path, transform_nl_path, part_folder, similarity_path, keep_intermediate_files):
	# fixed_masked_path, moving_registered_masked_path = utils.mask_images_intersection(fixed_resampled_path, moving_registered_path, threshold=1)
	with tempfile.TemporaryDirectory() as tmpdirname:
		output_folder = Path(tmpdirname)
		fixed_masked_path = output_folder / 'fixed_masked.nii.gz'

		subprocess.call(['animaMaskImage', '-i', fixed_resampled_path ,'-m', moving_registered_path, '-o', fixed_masked_path])

		elastix_transform_path = output_folder / 'TransformParametersFromAnima.0.txt'
		anima_to_elastix_rigid_transform(transform_path.resolve(), fixed_resampled_path, elastix_transform_path)

		subprocess.call(['elastix', '-f', fixed_resampled_path, '-m', moving_resampled_path, '-fMask', fixed_masked_path, '-out', output_folder, '-p', 'bspline_transform_parameters.txt', '-t0', elastix_transform_path])
		registered = sitk.ReadImage(str(output_folder / 'result.0.nii'))
		sitk.WriteImage(registered, str(moving_registered_nl_path))
		shutil.copyfile(output_folder / 'TransformParameters.0.txt', transform_nl_path)
		# subprocess.call(['transformix', '-in', fixed_resampled_path, '-out', output_folder, '-tp', transform_nl_path])
	return
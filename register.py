import argparse
from pathlib import Path
import shutil
import utils
from non_linear_registration import (register_non_linear_elastix_from_paths,
                                     register_non_linear_from_paths)
from rigid_registration import register_rigid_from_paths

parser = argparse.ArgumentParser(
    prog=__file__,
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    description="""Register rigidly and non-linearly two spinal cord images from a given patient. The method is designed to be robust to difference of sagital coverage and to partial overlap in the two images. However, images must have similar intensity characteristics (typically same sequence).""")

parser.add_argument('-f', '--fixed', required=True, help='Path to the reference image.')
parser.add_argument('-m', '--moving', required=True, help='Path to the image to register.')
parser.add_argument('-o', '--output_folder', default=None, help='Path to the output folder. Default is the current directory.')

utils.add_args(parser)

args = parser.parse_args()

utils.load_config(args.configuration)

output_folder = Path(args.output_folder) if args.output_folder else Path()
output_folder_path = output_folder
output_folder = output_folder / f"{output_folder.name}_tmp"
output_folder.mkdir(exist_ok=True, parents=True)

utils.set_debug(output_folder, args)

fixed_path = Path(args.fixed)
moving_path = Path(args.moving)
fixed_resampled_path = utils.set_output_file_path(args.fixed_resampled, output_folder, fixed_path.name.replace('.nii.gz', '_fixed_resampled.nii.gz'))
moving_resampled_path = utils.set_output_file_path(args.moving_resampled, output_folder, moving_path.name.replace('.nii.gz', '_moving_resampled.nii.gz'))
moving_registered_path = utils.set_output_file_path(args.moving_registered, output_folder, moving_path.name.replace('.nii.gz', '_registered.nii.gz'))
moving_registered_nl_path = utils.set_output_file_path(args.moving_registered_nl, output_folder, moving_path.name.replace('.nii.gz', '_registered_nl.nii.gz'))
moving_registered_nl_elastix_path = utils.set_output_file_path(args.moving_registered_nl, output_folder, moving_path.name.replace('.nii.gz', '_registered_nl_elastix.nii.gz'))
transform_path = output_folder / args.transform
transform_nl_path = output_folder / args.transform_nl
similarity_path = output_folder / args.similarity

fixed_reoriented_path = utils.reorient_and_convert_image(fixed_path, output_folder / fixed_path.name.replace('.nii.gz', '_fixed_reoriented.nii.gz'))
moving_reoriented_path = utils.reorient_and_convert_image(moving_path, output_folder / moving_path.name.replace('.nii.gz', '_moving_reoriented.nii.gz'))

if not (args.skip_rigid_if_exists and fixed_resampled_path.exists() and moving_resampled_path.exists() and moving_registered_path.exists() and transform_path.exists() and similarity_path.exists()):
    print('compute rigid registration...')
    register_rigid_from_paths(fixed_reoriented_path, moving_reoriented_path, fixed_resampled_path, moving_resampled_path, moving_registered_path, transform_path, similarity_path)

if 'anima' in args.non_linear_registration_tool:
    print('compute non linear registration with anima...')
    register_non_linear_from_paths(fixed_resampled_path, moving_resampled_path, moving_registered_path, moving_registered_nl_path, transform_path, transform_nl_path, output_folder, similarity_path, args.keep_intermediate_files)
    utils.resample_and_orient_image(moving_registered_nl_path, fixed_path)

if 'elastix' in args.non_linear_registration_tool:
    print('compute non linear registration with elastix...')
    register_non_linear_elastix_from_paths(fixed_resampled_path, moving_resampled_path, moving_registered_path, moving_registered_nl_elastix_path, transform_path, transform_nl_path, output_folder, similarity_path, args.keep_intermediate_files)
    utils.resample_and_orient_image(moving_registered_nl_path, fixed_path)

if args.create_previews:
    utils.create_sagittal_image_preview(fixed_resampled_path)
    utils.create_sagittal_image_preview(moving_registered_path)
    if 'anima' in args.non_linear_registration_tool:
        utils.create_sagittal_image_preview(moving_registered_nl_path)
    if 'elastix' in args.non_linear_registration_tool:
        utils.create_sagittal_image_preview(moving_registered_nl_elastix_path)


utils.symlink(output_folder / fixed_path.name.replace('.nii.gz', '_fixed.nii.gz'), fixed_path)
utils.symlink(output_folder / moving_path.name.replace('.nii.gz', '_moving.nii.gz'), moving_path)

# path to desired output from the temp directory
registered_bl_nl = output_folder / str(Path(args.moving).name).replace(".nii.gz", f"_registered_nl_matches_{Path(args.fixed).name}")
registered_bl = output_folder / str(Path(args.moving).name).replace(".nii.gz", "_registered_nl.nii.gz")

# copy only the desired registered image back to the output directory
shutil.copy(registered_bl_nl, output_folder_path)
# copy to keep the compactibility with the previous scripts
shutil.copy(registered_bl, output_folder_path)

# Remove temporary file if not specificed
if not args.keep_intermediate_files:
    shutil.rmtree(output_folder)


FROM python:3.10-slim

RUN apt-get update && apt-get install -y --no-install-recommends \
    git \
    wget \
    unzip \
    gcc \
    libgl1-mesa-glx \
    libglib2.0-0 \
    libsm6 \
    libxext6 \
    libxrender-dev \
    && rm -rf /var/lib/apt/lists/*

RUN pip install --upgrade pip

# installing sct
WORKDIR /sct
RUN git clone --single-branch -b master --depth 1 https://github.com/spinalcordtoolbox/spinalcordtoolbox.git && \
    cd spinalcordtoolbox && \
    ./install_sct -y -c
ENV PATH="$PATH:/sct/spinalcordtoolbox/bin"

# installing Anima
WORKDIR /Anima
RUN wget https://github.com/Inria-Empenn/Anima-Public/releases/download/v4.2/Anima-Ubuntu-4.2.zip && \
    unzip Anima-Ubuntu-4.2.zip && \
    ls -la Anima-Binaries-4.2 && \
    rm Anima-Ubuntu-4.2.zip && \
    git clone --single-branch -b master --depth 1 https://github.com/Inria-Empenn/Anima-Scripts-Public.git && \
    cd Anima-Scripts-Public && \
    python configure.py -a /Anima/Anima-Binaries-4.2/ -s /Anima/Anima-Scripts-Public/
ENV PATH="/Anima/Anima-Binaries-4.2:$PATH"

WORKDIR /app_sc_registration
COPY . .

RUN pip install --no-cache-dir -r requirements.txt 

# Create separate directories for input and output
RUN mkdir -p /data/input /data/output


CMD ["python3", "register.py", "-f", "/data/input/fu.nii.gz", "-m", "/data/input/bl.nii.gz", "-o", "/data/output"]
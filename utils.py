import json
import math
import re
import sys
import tempfile
from functools import reduce
from pathlib import Path

import cv2 as cv
import numpy as np
import pandas
import SimpleITK as sitk
import yaml
from PIL import Image

from anima_utils import *

orb = cv.ORB_create()
orb_bf = cv.BFMatcher(cv.NORM_HAMMING, crossCheck=True)

# sift = cv.SIFT_create(contrastThreshold=0.03, edgeThreshold=10)
sift = cv.SIFT_create()
sift_bf = cv.BFMatcher(crossCheck=True)

configuration = None

def load_config(config_file_path):
	global configuration
	with open(config_file_path, "r") as f:
		try:
			configuration = yaml.safe_load(f)
		except yaml.YAMLError as exc:
			sys.exit(exc)

# Dirty shortcut to avoid passing output_folder in all functions for debugging purpose 
def set_debug(output_folder, args):
	configuration['output_folder'] = output_folder
	configuration['debug'] = args.debug

def sort_folder(folder_path):
	return sorted([d for d in folder_path.iterdir() if d.is_dir()], key=lambda x: int(x.name) if x.name.isdigit() else re.search(r'\d+', x.name).group())

def reorient_image(image_path, output_path, orientation=None):
	orientation = orientation or configuration['orientation']
	call([animaConvertImage, '-i', image_path, '-R', orientation, '-o', output_path])
	return output_path


def reorient_and_convert_image(input_path, output_path=None, orientation='ASL', image_type=sitk.sitkFloat64):
	output_path = output_path or input_path
	image = sitk.ReadImage(str(input_path))
	doif = sitk.DICOMOrientImageFilter()
	doif.SetDesiredCoordinateOrientation(orientation)
	image = doif.Execute(image)
	image = sitk.Cast(image, image_type)
	sitk.WriteImage(image, str(output_path))
	return output_path


def compute_similarity(image1, image2, n_best_matches=None, max_distance=None, match_image_path=None):
	n_best_matches = n_best_matches or configuration['n_best_matches']
	max_distance = max_distance or configuration['similarity_features_max_distance']
	image1_data = sitk.GetArrayFromImage(image1)
	image2_data = sitk.GetArrayFromImage(image2)
	image1_data_0 = convert_to_uint8(image1_data[image1_data.shape[0]//2,:,:])
	image2_data_0 = convert_to_uint8(image2_data[image2_data.shape[0]//2,:,:])

	distance = 0
	# find the keypoints and descriptors
	for detector, matcher, detector_name in [(sift, sift_bf, 'sift'), (orb, orb_bf, 'orb')]:

		kp1, des1 = detector.detectAndCompute(image1_data_0,None)
		kp2, des2 = detector.detectAndCompute(image2_data_0,None)
		
		if des1 is None or des2 is None or len(des1) == 0 or len(des2) == 0 or des1.dtype != des2.dtype:
			return 0
		
		matches = matcher.match(des1,des2)

		matches = sorted(matches, key = lambda x:x.distance)
		best_matches = []
		for i, match in enumerate(matches[:n_best_matches]):
			point1 = kp1[match.queryIdx].pt
			point2 = kp2[match.trainIdx].pt
			delta = [point1[i] - point2[i] for i in range(2)]
			dist = delta[0] * delta[0] + delta[1] * delta[1]
			if dist < max_distance*max_distance:
				distance -= 1
				best_matches.append(match)

		if match_image_path:
			match_image = cv.drawMatches(image1_data_0, kp1, image2_data_0, kp2, best_matches[:n_best_matches], None, flags=cv.DrawMatchesFlags_NOT_DRAW_SINGLE_POINTS)
			cv.imwrite(str(match_image_path.parent / match_image_path.name.replace('.png', f'_{detector_name}.png')), match_image)
			match_image = cv.drawMatches(image1_data_0, kp1, image2_data_0, kp2, matches[:2*n_best_matches], None, flags=cv.DrawMatchesFlags_NOT_DRAW_SINGLE_POINTS)
			cv.imwrite(str(match_image_path.parent / match_image_path.name.replace('.png', f'_{detector_name}_all.png')), match_image)
	return distance

def compute_similarity_from_path(image1_path, image2_path, n_best_matches=None, max_distance=None, match_image_path=None):
	image1 = sitk.ReadImage(str(image1_path))
	image2 = sitk.ReadImage(str(image2_path))
	return compute_similarity(image1, image2, n_best_matches, max_distance, match_image_path)

def convert_to_uint8(image_data):
	min_value = np.min(image_data)
	max_value = np.max(image_data)
	image_data = (255.0 * (image_data - min_value)) / float(max_value - min_value) if max_value > min_value else image_data
	return image_data.astype(np.uint8)

def apply_transform(fixed, moving, transform, interpolation=sitk.sitkLinear):
	resampler = sitk.ResampleImageFilter()
	resampler.SetReferenceImage(fixed)
	resampler.SetInterpolator(interpolation)
	resampler.SetTransform(transform)
	return resampler.Execute(moving)

# Does not work when input transforms are CompositeTransforms!
def apply_transforms_anima(fixed_path, moving_path, transform_paths, transform_serie_path, output_path, keep_intermediate_files=False):
	transform_args = [['-i', transform_path] for transform_path in transform_paths]
	transform_args = [item for sublist in transform_args for item in sublist]
	call([animaTransformSerieXmlGenerator] + transform_args + ['-o', transform_serie_path])
	call([animaApplyTransformSerie, '-i', moving_path, '-t', transform_serie_path, '-g', fixed_path, '-o', output_path])
	if not keep_intermediate_files:
		transform_serie_path.unlink()
	return output_path

def convert_transform(path):
	with tempfile.TemporaryDirectory() as tmpdirname:
		output_path = Path(tmpdirname) / 'transform.nii.gz'
		call([animaDenseTransformArithmetic, '-i', path, '-o', output_path, '-E'])
		return sitk.DisplacementFieldTransform(sitk.ReadImage(str(output_path)))

def read_transform(path):
	return convert_transform(path) if path.name.endswith('.nii.gz') else sitk.ReadTransform(str(path))

def apply_transforms_sitk(fixed_path, moving_path, transform_paths, output_path, interpolation=sitk.sitkLinear):
	fixed = sitk.ReadImage(str(fixed_path))
	moving = sitk.ReadImage(str(moving_path))
	transform = sitk.CompositeTransform([read_transform(transform_path) for transform_path in transform_paths])
	output = apply_transform(fixed, moving, transform, interpolation)
	sitk.WriteImage(output, str(output_path))
	return output_path

def set_spacing(image, output_spacing):
	resampler = sitk.ResampleImageFilter()
	resampler.SetInterpolator(sitk.sitkLinear)
	resampler.SetOutputDirection(image.GetDirection())
	resampler.SetOutputOrigin(image.GetOrigin())
	size = image.GetSize()
	spacing = image.GetSpacing()
	resampler.SetSize([math.ceil(size[i] * spacing[i] / output_spacing[i]) for i in range(3)])
	resampler.SetOutputSpacing(output_spacing)
	return resampler.Execute(image)

def get_patch(image, source_index, size):
	size = [int(s) for s in size]
	source_index = [int(s) for s in source_index]
	pif = sitk.PasteImageFilter()
	copy_image = sitk.Image(size, image.GetPixelID())
	copy_image.SetSpacing(image.GetSpacing())
	copy_image.SetDirection(image.GetDirection())
	copy_image.SetOrigin(image.TransformIndexToPhysicalPoint(source_index))
	pif.SetSourceIndex(source_index)
	pif.SetSourceSize(size)
	pif.SetDestinationIndex([0, 0, 0])

	return pif.Execute(copy_image, image)

def get_bounding_box_origin(bb):
	return [bb[0], bb[2], bb[4]]

def get_bounding_box_end(bb):
	return [bb[1], bb[3], bb[5]]

def get_bounding_box_size_from_ends(origin, end):
	return [end[0] - origin[0] + 1, end[1] - origin[1] + 1, end[2] - origin[2] + 1]

def get_bounding_box_from_ends(origin, end):
	return [origin[0], end[0], origin[1], end[1], origin[2], end[2]]

def get_bounding_box(image):
	origin = list(image.GetOrigin())
	end = list(image.TransformIndexToPhysicalPoint(image.GetSize()))
	return get_bounding_box_from_ends(origin, end)

def get_bounding_box_from_content(image):
	mask_data = sitk.GetArrayFromImage(image)
	mask_data = (mask_data >= 1e-6).astype(np.uint8)
	ccifilter = sitk.ConnectedComponentImageFilter()
	ccifilter.FullyConnectedOn()
	lsifilter = sitk.LabelStatisticsImageFilter()
	mask_binary = sitk.GetImageFromArray(mask_data.astype(np.uint8))
	mask_binary.CopyInformation(image)
	labeled = ccifilter.Execute(mask_binary)
	lsifilter.Execute(labeled, labeled)
	n_components = ccifilter.GetObjectCount()

	bounding_boxes = [list(lsifilter.GetBoundingBox(i)) for i in range(1, n_components + 1)]
	return get_union(bounding_boxes)

def get_union(bounding_boxes):
	origins = [get_bounding_box_origin(bb) for bb in bounding_boxes]
	ends = [get_bounding_box_end(bb) for bb in bounding_boxes]
	origin = reduce(np.minimum, origins)
	end = reduce(np.maximum, ends)
	return get_bounding_box_from_ends(origin, end)

def get_intersection(bounding_boxes):
	origins = [get_bounding_box_origin(bb) for bb in bounding_boxes]
	ends = [get_bounding_box_end(bb) for bb in bounding_boxes]
	origin = reduce(np.maximum, origins)
	end = reduce(np.minimum, ends)
	return get_bounding_box_from_ends(origin, end)

def get_patch_from_bounding_box(image, bounding_box):
	origin = get_bounding_box_origin(bounding_box)
	end = get_bounding_box_end(bounding_box)
	size = get_bounding_box_size_from_ends(origin, end)
	return get_patch(image, origin, size)

def edit_json(json_path, key, value):
	original_data = {}
	if json_path.exists():
		with open(json_path, 'r') as f:
			original_data = json.load(f)
	original_data[key] = value
	with open(json_path, 'w') as f:
		json.dump(original_data, f)

def set_output_file_path(path_string, output_folder, default_name):
	if path_string is None: return output_folder / default_name
	path = Path(path_string)
	return output_folder / path if path.parent == Path() else path

def symlink(path, target):
	path.parent.mkdir(exist_ok=True, parents=True)
	if not path.exists():
		path.symlink_to(target)
	return

def create_union_image(images):
	if len(images) == 0: return None

	bounding_boxes = [get_bounding_box(image) for image in images]
	union = get_union(bounding_boxes)
	origin = get_bounding_box_origin(union)
	end = get_bounding_box_end(union)
	
	image = images[0]

	origin_index = image.TransformPhysicalPointToIndex(origin)
	end_index = image.TransformPhysicalPointToIndex(end)
	size = [end_index[i] - origin_index[i] for i in range(3)]

	union_image = sitk.Image(size, image.GetPixelID())
	union_image.SetOrigin(origin)
	union_image.SetDirection(image.GetDirection())
	union_image.SetSpacing(image.GetSpacing())
	return union_image

def paste_image(target_image, image):

	pif = sitk.PasteImageFilter()
	pif.SetSourceIndex([0, 0, 0])
	pif.SetSourceSize(image.GetSize())
	pif.SetDestinationIndex(target_image.TransformPhysicalPointToIndex(image.GetOrigin()))

	return pif.Execute(target_image, image)

def get_masks_union(mask1_path, mask2_path, output_path):
	mask1 = sitk.ReadImage(str(mask1_path))
	mask2 = sitk.ReadImage(str(mask2_path))
	union_image = create_union_image([mask1, mask2])
	mask1 = paste_image(union_image, mask1)
	mask2 = paste_image(union_image, mask2)
	mask1_data = sitk.GetArrayFromImage(mask1)
	mask2_data = sitk.GetArrayFromImage(mask2)
	mask_data = np.logical_or(mask1_data, mask2_data)
	mask_image = sitk.GetImageFromArray(mask_data.astype(np.uint8))
	mask_image.CopyInformation(union_image)
	sitk.WriteImage(mask_image, str(output_path))
	return output_path

def mask_images_intersection(fixed_path, moving_path, fixed_masked_path=None, moving_masked_path=None, threshold=2):
	fixed_masked_path = fixed_masked_path or fixed_path
	moving_masked_path = moving_masked_path or moving_path

	fixed = sitk.ReadImage(str(fixed_path))
	fixed_data = sitk.GetArrayFromImage(fixed)
	fixed_data_uint8 = convert_to_uint8(fixed_data)

	moving = sitk.ReadImage(str(moving_path))
	moving_data = sitk.GetArrayFromImage(moving)
	moving_data_uint8 = convert_to_uint8(moving_data)
	mask_data = np.logical_and(fixed_data_uint8 >= threshold, moving_data_uint8 >= threshold)

	moving_data[mask_data == False] = 0
	moving_cropped = sitk.GetImageFromArray(moving_data)
	moving_cropped.CopyInformation(moving)
	sitk.WriteImage(moving_cropped, str(moving_masked_path))

	fixed_data[mask_data == False] = 0
	fixed_cropped = sitk.GetImageFromArray(fixed_data)
	fixed_cropped.CopyInformation(fixed)
	sitk.WriteImage(fixed_cropped, str(fixed_masked_path))
	return fixed_masked_path, moving_masked_path

def create_sagittal_image_preview(image_path, preview_path=None):
	image = sitk.ReadImage(str(image_path))
	image = set_spacing(image, [0.5, 0.5, 0.5])
	image_data = convert_to_uint8(sitk.GetArrayFromImage(image))
	preview_data = image_data[image_data.shape[0] // 2, :, :]
	pillow_image = Image.fromarray(preview_data)
	preview_path = preview_path or image_path.parent / image_path.name.replace('.nii.gz', '.png')
	pillow_image.save(str(preview_path), format='png')
	return preview_path

def resample_and_orient_image(input_image_path, reference_image_path):
	input_image = sitk.ReadImage(input_image_path)
	reference_image = sitk.ReadImage(reference_image_path)

	# extract and replace the name
    # Default output path if not provided
	referece_name = Path(reference_image_path).name
	output_path = str(input_image_path).replace(".nii.gz", f"_matches_{referece_name}")
	output_path = Path(output_path)

	resampler = sitk.ResampleImageFilter()
	size = reference_image.GetSize()
	spacing = reference_image.GetSpacing()
	resampler.SetSize([ int(size[0] * spacing[0] / reference_image.GetSpacing()[0]),
		       int(size[1] * spacing[1] / reference_image.GetSpacing()[1]),
	           int(size[2] * spacing[2] / reference_image.GetSpacing()[2])])
	resampler.SetOutputSpacing(reference_image.GetSpacing())
	resampler.SetOutputOrigin(reference_image.GetOrigin())
	resampler.SetOutputDirection(reference_image.GetDirection())
	resampler.SetInterpolator(sitk.sitkLinear)
	resampler.SetOutputPixelType(input_image.GetPixelID()) # to maintain data propeties of the input image
	result = resampler.Execute(input_image)
	sitk.WriteImage(result, output_path)

def add_args(parser):
	parser.add_argument('-fr', '--fixed_resampled', default=None, help='Output path of the resampled reference image. Default is fixed_name_resampled.nii.gz.')
	parser.add_argument('-mr', '--moving_resampled', default=None, help='Output path of the resampled image to register. Default is moving_name_resampled.nii.gz.')
	parser.add_argument('-mrg', '--moving_registered', default=None, help='Output path of the rigidly registered image. Default is moving_name_registered.nii.gz.')
	parser.add_argument('-mrgnl', '--moving_registered_nl', default=None, help='Output path of the non linearly registered image. Default is moving_name_registered_nl.nii.gz.')
	parser.add_argument('-tr', '--transform', default='transform.txt', help='Output path of the rigid transformation.')
	parser.add_argument('-trnl', '--transform_nl', default='transform.nii.gz', help='Output path of the non linear transformation.')
	parser.add_argument('-sim', '--similarity', default='similarity.txt', help='Output path of the similarity file.')
	parser.add_argument('-kif', '--keep_intermediate_files', action='store_true', help='Keep intermediate files.')
	parser.add_argument('-sr', '--skip_rigid_if_exists', action='store_true', help='Skip rigid registration if output files already exist.')
	parser.add_argument('-nlrt', '--non_linear_registration_tool', nargs='+', help='Registration tool to use for the non linear registration. Anima will compute a dense deformation field, and elastix will compute a bspline transformation. Use anima, elastix, or both. Elastix registration will be prefixed with _elastix.', default=['anima'], choices=['anima', 'elastix'])
	parser.add_argument('-cp', '--create_previews', action='store_true', help='Create image previews.')
	parser.add_argument('-cfg', '--configuration', default='config.yml', help='Path to the configuration file.')
	parser.add_argument('-dbg', '--debug', action='store_true', help='Debug will output previews of the feature matches.')
	return

def save_similarities(patient_name, part_name, similarity_path, similarity_records, output_path):
	if similarity_path.exists():
		with open(similarity_path, 'r') as f:
			similarity_record = {'patient': patient_name, 'part': part_name}
			similarity = json.load(f)
			for key in similarity.keys():
				similarity_record[key] = similarity[key]
			similarity_records.append(similarity_record)
			similarity_df = pandas.DataFrame.from_records(similarity_records)
			similarity_df.to_csv(output_path, index=False)
	return
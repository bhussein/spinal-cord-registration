import argparse
import json
from pathlib import Path

import pandas

import elastix_registration
import non_linear_registration
import rigid_registration
import utils

# from rigid_registration import register_rigid_from_paths
# from non_linear_registration import register_non_linear_from_paths

parser = argparse.ArgumentParser(
    prog=__file__,
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    description="""Register spinal cord images""")

parser.add_argument('-i', '--input_folder', required=True, help='Path to the patients folder. See the file structure in Readme.md.')
parser.add_argument('-o', '--output_folder', default=None, help='Path to the output folder. Default is the current directory.')
parser.add_argument('-p', '--patients', default=None, help='Names of the patients to process. Process all patients by default.', nargs='+')
parser.add_argument('-pr', '--primus', action='store_true', help='Use the primus file structure.')

utils.add_args(parser)

args = parser.parse_args()

utils.load_config(args.configuration)
patients_folder = Path(args.input_folder)

patients = utils.sort_folder(patients_folder)

if args.patients:
    patients = [p for p in patients if p.name in args.patients]

output_folder = Path(args.output_folder) if args.output_folder else Path()
output_folder.mkdir(exist_ok=True, parents=True)

def find_first_part(examination_dates, part, date_index):
	first_part_index = 0
	first_part = examination_dates[first_part_index] / part.name
	while not first_part.exists() and first_part_index < date_index:
		first_part_index += 1
		first_part = examination_dates[first_part_index] / part.name
	if not first_part.exists() or first_part == part:
		return None, None
	return first_part, first_part_index

if args.create_previews:
    output_preview_folder = output_folder.parent / f'{output_folder.name}_preview'
    output_preview_nl_folder = output_folder.parent / f'{output_folder.name}_preview_nl'

similarity_records = []

def register_default():
    for patient in patients:
        print(patient.name)
        examination_dates = sorted(list(patient.iterdir()))

        for date_index, examination_date in enumerate(examination_dates):
            print('   ', examination_date.name)
            parts = sorted(list(examination_date.glob('*.nii.gz')))
            # parts = [part for part in parts if 'combined' not in part.name]
            parts = [part for part in parts if part.name[:-len('.nii.gz')].isdigit()]

            part_images = []

            for part in parts:
                part_name = part.name.replace('.nii.gz', '')
                part_folder = output_folder / patient.name / part_name
                part_folder.mkdir(exist_ok=True, parents=True)
                utils.set_debug(part_folder, args)
                fixed_reoriented_path = utils.reorient_image(part, part_folder / f'{examination_date.name}_reoriented.nii.gz')

                first_part, first_part_index = find_first_part(examination_dates, part, date_index)
                if first_part is None: continue

                moving_reoriented_path = part_folder / f'{examination_dates[first_part_index].name}_reoriented.nii.gz'

                fixed_resampled_path = part_folder / args.fixed_resampled if args.fixed_resampled else part_folder / fixed_reoriented_path.name.replace('.nii.gz', '_fixed_resampled.nii.gz')
                moving_resampled_path = part_folder / args.moving_resampled if args.moving_resampled else part_folder / moving_reoriented_path.name.replace('.nii.gz', '_moving_resampled.nii.gz')
                moving_registered_path = part_folder / args.moving_registered if args.moving_registered else part_folder / moving_reoriented_path.name.replace('.nii.gz', '_registered.nii.gz')
                moving_registered_nl_path = part_folder / args.moving_registered_nl if args.moving_registered_nl else part_folder / moving_reoriented_path.name.replace('.nii.gz', '_registered_nl.nii.gz')
                moving_registered_nl_elastix_path = part_folder / args.moving_registered_nl if args.moving_registered_nl else part_folder / moving_reoriented_path.name.replace('.nii.gz', '_registered_nl_elastix.nii.gz')
                transform_path = part_folder / args.transform
                transform_nl_path = part_folder / args.transform_nl
                similarity_path = part_folder / args.similarity

                if not (args.skip_rigid_if_exists and fixed_resampled_path.exists() and moving_resampled_path.exists() and moving_registered_path.exists() and transform_path.exists() and similarity_path.exists()):
                    print('      compute rigid registration...')
                    rigid_registration.register_rigid_from_paths(fixed_reoriented_path, moving_reoriented_path, fixed_resampled_path, moving_resampled_path, moving_registered_path, transform_path, similarity_path)
                    # elastix_registration.register_rigid_from_paths(fixed_reoriented_path, moving_reoriented_path, fixed_resampled_path, moving_resampled_path, moving_registered_path, transform_path, similarity_path)
                    # fixed_resampled_path = fixed_reoriented_path
                    # moving_resampled_path = moving_reoriented_path

                if 'anima' in args.non_linear_registration_tool:
                    print('      compute non linear registration with anima...')
                    non_linear_registration.register_non_linear_from_paths(fixed_resampled_path, moving_resampled_path, moving_registered_path, moving_registered_nl_path, transform_path, transform_nl_path, part_folder, similarity_path, args.keep_intermediate_files)
                    utils.resample_and_orient_image(moving_registered_nl_path, fixed_reoriented_path)

                if 'elastix' in args.non_linear_registration_tool:
                    print('      compute non linear registration with elastix...')
                    non_linear_registration.register_non_linear_elastix_from_paths(fixed_resampled_path, moving_resampled_path, moving_registered_path, moving_registered_nl_elastix_path, transform_path, transform_nl_path, part_folder, similarity_path, args.keep_intermediate_files)
                    utils.resample_and_orient_image(moving_registered_nl_path, fixed_reoriented_path)

                if args.create_previews:
                    fixed_resampled_png_path = utils.create_sagittal_image_preview(fixed_resampled_path)
                    moving_registered_png_path = utils.create_sagittal_image_preview(moving_registered_path)

                    utils.symlink(output_preview_folder / f'{patient.name}_{part_name}_{fixed_resampled_png_path.name}', fixed_resampled_png_path)
                    utils.symlink(output_preview_folder / f'{patient.name}_{part_name}_{moving_registered_png_path.name}', moving_registered_png_path)
                    utils.symlink(output_preview_nl_folder / f'{patient.name}_{part_name}_{fixed_resampled_png_path.name}', fixed_resampled_png_path)

                    if 'anima' in args.non_linear_registration_tool:
                        moving_registered_nl_png_path = utils.create_sagittal_image_preview(moving_registered_nl_path)
                        utils.symlink(output_preview_nl_folder / f'{patient.name}_{part_name}_{moving_registered_nl_png_path.name}', moving_registered_nl_png_path)
                    if 'elastix' in args.non_linear_registration_tool:
                        moving_registered_nl_elastix_png_path = utils.create_sagittal_image_preview(moving_registered_nl_elastix_path)
                        utils.symlink(output_preview_nl_folder / f'{patient.name}_{part_name}_{moving_registered_nl_elastix_png_path.name}', moving_registered_nl_elastix_png_path)
                    

                if not args.keep_intermediate_files:
                    fixed_reoriented_path.unlink()
                    moving_reoriented_path.unlink()

                utils.symlink(part_folder / f'{examination_date.name}_fixed.nii.gz', part)
                utils.symlink(part_folder / f'{examination_dates[first_part_index].name}_moving.nii.gz', first_part)

                # utils.save_similarities(patient.name, part.name, similarity_path, similarity_records, output_folder / 'similarities.csv')

def get_first_nifti(part, modality, orientation='Sag'):
    niftis = list(part.glob(modality + orientation + '*.nii.gz')) if part.exists() else []
    return niftis[0] if len(niftis)>0 else None

def find_first_part_primus(modality, date_index, examination_dates, part_name):
    first_index = 0
    while first_index < date_index:
        first_nifti = get_first_nifti(examination_dates[first_index] / part_name, modality, 'Sag')
        if first_nifti is not None: return first_nifti, examination_dates[first_index]
        first_index += 1
    return None, None

def register_primus():
    for patient in patients:
        print(patient.name)
        examination_dates = sorted(list(patient.iterdir()))

        for date_index, examination_date in enumerate(examination_dates):
            print('   ', examination_date.name)
            
            for part in sorted(list(examination_date.iterdir())):
                
                images = sorted(list(part.iterdir()))
                
                modalities = set([image.name.split('Sag')[0] for image in images if 'Sag' in image.name])
                part_folder = None
                for modality in modalities:
                    
                    nifti = get_first_nifti(part, modality, 'Sag')
                    if nifti is None: continue

                    first_nifti, first_examination_date = find_first_part_primus(modality, date_index, examination_dates, part.name)
                    if first_nifti is None: continue

                    part_folder = output_folder / patient.name / f'{first_examination_date.name}_to_{examination_date.name}' / part.name
                    part_folder.mkdir(exist_ok=True, parents=True)
                    utils.set_debug(part_folder, args)

                    fixed_reoriented_path = utils.reorient_image(nifti, part_folder / f'{modality}_{examination_date.name}_reoriented.nii.gz')
                    moving_reoriented_path = utils.reorient_image(first_nifti, part_folder / f'{modality}_{first_examination_date.name}_reoriented.nii.gz')

                    fixed_resampled_path = part_folder / fixed_reoriented_path.name.replace('_reoriented.nii.gz', f'_resampled.nii.gz')
                    moving_resampled_path = part_folder / moving_reoriented_path.name.replace('_reoriented.nii.gz', '_resampled.nii.gz')
                    moving_registered_path = part_folder / moving_reoriented_path.name.replace('_reoriented.nii.gz', '_registered.nii.gz')
                    moving_registered_nl_path = part_folder / moving_reoriented_path.name.replace('_reoriented.nii.gz', '_registered_nl.nii.gz')
                    moving_registered_nl_elastix_path = part_folder / moving_reoriented_path.name.replace('_reoriented.nii.gz', '_registered_nl_elastix.nii.gz')
                    transform_path = part_folder / f'{modality}_transform.txt'
                    transform_nl_path = part_folder / f'{modality}_nl_transform.nii.gz'
                    similarity_path = part_folder / f'{modality}_similarity.txt'

                    if not (args.skip_rigid_if_exists and fixed_resampled_path.exists() and moving_resampled_path.exists() and moving_registered_path.exists() and transform_path.exists() and similarity_path.exists()):
                        print('      compute rigid registration...')
                        rigid_registration.register_rigid_from_paths(fixed_reoriented_path, moving_reoriented_path, fixed_resampled_path, moving_resampled_path, moving_registered_path, transform_path, similarity_path)
                        # elastix_registration.register_rigid_from_paths(fixed_reoriented_path, moving_reoriented_path, fixed_resampled_path, moving_resampled_path, moving_registered_path, transform_path, similarity_path)
                        # fixed_resampled_path = fixed_reoriented_path
                        # moving_resampled_path = moving_reoriented_path

                    if 'anima' in args.non_linear_registration_tool:
                        print('      compute non linear registration with anima...')
                        non_linear_registration.register_non_linear_from_paths(fixed_resampled_path, moving_resampled_path, moving_registered_path, moving_registered_nl_path, transform_path, transform_nl_path, part_folder, similarity_path, args.keep_intermediate_files)
                    if 'elastix' in args.non_linear_registration_tool:
                        print('      compute non linear registration with elastix...')
                        non_linear_registration.register_non_linear_elastix_from_paths(fixed_resampled_path, moving_resampled_path, moving_registered_path, moving_registered_nl_elastix_path, transform_path, transform_nl_path, part_folder, similarity_path, args.keep_intermediate_files)

                    part_name = part.name

                    if args.create_previews:
                        fixed_resampled_png_path = utils.create_sagittal_image_preview(fixed_resampled_path)
                        moving_registered_png_path = utils.create_sagittal_image_preview(moving_registered_path)

                        utils.symlink(output_preview_folder / f'{patient.name}_{part_name}_{date_index}_{fixed_resampled_png_path.name}', fixed_resampled_png_path)
                        utils.symlink(output_preview_folder / f'{patient.name}_{part_name}_{date_index}_{moving_registered_png_path.name}', moving_registered_png_path)
                        utils.symlink(output_preview_nl_folder / f'{patient.name}_{part_name}_{date_index}_{fixed_resampled_png_path.name}', fixed_resampled_png_path)

                        if 'anima' in args.non_linear_registration_tool:
                            moving_registered_nl_png_path = utils.create_sagittal_image_preview(moving_registered_nl_path)
                            utils.symlink(output_preview_nl_folder / f'{patient.name}_{part_name}_{date_index}_{moving_registered_nl_png_path.name}', moving_registered_nl_png_path)
                        if 'elastix' in args.non_linear_registration_tool:
                            moving_registered_nl_elastix_png_path = utils.create_sagittal_image_preview(moving_registered_nl_elastix_path)
                            utils.symlink(output_preview_nl_folder / f'{patient.name}_{part_name}_{date_index}_{moving_registered_nl_elastix_png_path.name}', moving_registered_nl_elastix_png_path)
                        

                    if not args.keep_intermediate_files:
                        fixed_reoriented_path.unlink()
                        moving_reoriented_path.unlink()

                    utils.symlink(part_folder / f'{examination_date.name}_{modality}_{date_index}_fixed.nii.gz', nifti)
                    utils.symlink(part_folder / f'{first_examination_date.name}_{modality}_{date_index}_moving.nii.gz', first_nifti)

                    # utils.save_similarities(patient.name, part.name, similarity_path, similarity_records, output_folder / 'similarities.csv')

                if part_folder is not None:
                    for image in images:
                        utils.symlink(part_folder / image.name, image)

if args.primus:
    register_primus()
else:
    register_default()
import argparse
import math
import utils
from anima_utils import *
import SimpleITK as sitk

from rigid_registration import get_images_same_size, get_images_same_spacing, register_anima_from_image_and_read_outputs
from non_linear_registration import register_non_linear_from_paths, register_anima_non_linear, bspline_intra_modal_registration

def get_image_patches(image, patch_size, axis, min_overlap):
	images = []
	source_index = [0, 0, 0]
	size = list(image.GetSize())
	length = size[axis]
	size[axis] = min(patch_size, length)
	n = math.ceil( (length - min_overlap * patch_size)  / (patch_size*(1-min_overlap)) )
	for i in range(n):
		source_index[axis] = (i * (length - patch_size)) // (n - 1) if n>1 else 0
		images.append(utils.get_patch(image, source_index, size))
	return images


# Patches will be extracted from fixed_path and moving_path, a registration will be computed for each pair of patches
def register_patches(fixed, moving, patch_size_mm=100, patch_overlap=0.75, axis=None, similarity_early_stop_threshold=None):
	axis = axis or utils.configuration['axis']
	similarity_early_stop_threshold = similarity_early_stop_threshold or utils.configuration['similarity_early_stop_threshold']
	fixed, moving = get_images_same_spacing(fixed, moving, axis)
	
	axis_spacing = fixed.GetSpacing()[axis]
	patch_size = patch_size_mm // axis_spacing

	# NOT SO USEFUL!
	fixed_cropped, moving_cropped = get_images_same_size(fixed, moving, axis)
	
	fixed_patches = get_image_patches(fixed_cropped, patch_size, axis, patch_overlap)
	moving_patches = get_image_patches(moving_cropped, patch_size, axis, patch_overlap)

	fixed_path = utils.configuration['output_folder'] / f'fixed.nii.gz'
	moving_path = utils.configuration['output_folder'] / f'moving.nii.gz'

	sitk.WriteImage(fixed, str(fixed_path))
	sitk.WriteImage(moving, str(moving_path))
	
	registered = sitk.Image(fixed.GetSize(), sitk.sitkFloat64)
	registered.CopyInformation(fixed)

	registered_nl = sitk.Image(fixed.GetSize(), sitk.sitkFloat64)
	registered_nl.CopyInformation(fixed)
	
	pif = sitk.PasteImageFilter()

	for i, fixed_patch in enumerate(fixed_patches):

		registrations = []
		for j, moving_patch in enumerate(moving_patches):
			print(f'    register patches {i} and {j}...')
			moving_patch_registered, transform = register_anima_from_image_and_read_outputs(fixed_patch, moving_patch)
			moving_registered = utils.apply_transform(fixed, moving, transform)
			
			similarity = utils.compute_similarity(fixed, moving_registered, match_image_path=utils.configuration['output_folder'] / f'image_{i}_on_{j}.png' if utils.configuration['debug'] else None)
			
			# Recompute similarity on patches only (not full images) to better visualize what has been registered
			patch_similarity = utils.compute_similarity(fixed_patch, moving_patch_registered, match_image_path=utils.configuration['output_folder'] / f'patch_{i}_on_{j}.png' if utils.configuration['debug'] else None)
			
			registrations.append({ 
				'fixed_patch_number': i,
				'moving_patch_number': j,
				'transform': transform, 
				'name': 'patches',
				'similarity': similarity,
				'patch_similarity': patch_similarity,
				'fixed': fixed,
				'moving': moving,
				'moving_registered': moving_registered,
				'moving_patch_registered': moving_patch_registered,
			})

			if similarity_early_stop_threshold != 'disabled' and similarity <= similarity_early_stop_threshold:
				break
		
		registrations.sort(key=lambda x: x['similarity'])

		best_registration = registrations[0]

		pi = best_registration['fixed_patch_number']
		pj = best_registration['moving_patch_number']

		fixed_patch_path = utils.configuration['output_folder'] / f'fixed_patch_{pi}_on_{pj}.nii.gz'
		moving_registered_patch_path = utils.configuration['output_folder'] / f'moving_registered_patch_{pi}_on_{pj}.nii.gz'
		
		moving_patch_registered = best_registration['moving_patch_registered']

		sitk.WriteImage(best_registration['moving_patch_registered'], str(moving_registered_patch_path))
		sitk.WriteImage(fixed_patch, str(fixed_patch_path))

		moving_registered_path = utils.configuration['output_folder'] / f'moving_registered_{pi}_on_{pj}.nii.gz'
		
		sitk.WriteImage(best_registration['moving_registered'], str(moving_registered_path))

		# register non linearly moving patch on fixed one
		# # moving_registered_nl_path = utils.configuration['output_folder'] / f'moving_registered_nl_patch_{pi}_on_{pj}.nii.gz'
		moving_registered_nl_path = utils.configuration['output_folder'] / f'moving_registered_nl_{pi}_on_{pj}.nii.gz'
		transform_nl_path = utils.configuration['output_folder'] / f'patch_{pi}_to_{pj}_transform_nl.nii.gz'
		
		# # fixed_patch_masked_path = utils.configuration['output_folder'] / f'fixed_patch_masked_{pi}_on_{pj}.nii.gz'
		# # moving_registered_masked_path = utils.configuration['output_folder'] / f'moving_registered_patch_masked_{pi}_on_{pj}.nii.gz'

		# # utils.mask_images_intersection(fixed_patch_path, moving_registered_patch_path, fixed_patch_masked_path, moving_registered_masked_path, threshold=1)
		# # register_anima_non_linear(fixed_patch_masked_path, moving_registered_masked_path, moving_registered_nl_path, transform_nl_path)

		# fixed_masked_path = utils.configuration['output_folder'] / f'fixed_masked_{pi}_on_{pj}.nii.gz'
		# moving_registered_masked_path = utils.configuration['output_folder'] / f'moving_registered_masked_{pi}_on_{pj}.nii.gz'

		transform_path = utils.configuration['output_folder'] / f'patch_{pi}_to_{pj}_transform.txt'
		similarity_path = utils.configuration['output_folder'] / f'patch_{pi}_to_{pj}_similarity.json'

		sitk.WriteTransform(best_registration['transform'], str(transform_path))

		register_non_linear_from_paths(fixed_path, moving_path, moving_registered_path, moving_registered_nl_path, transform_path, transform_nl_path, utils.configuration['output_folder'], similarity_path, keep_intermediate_files=False)

		# utils.mask_images_intersection(fixed_path, moving_registered_path, fixed_masked_path, moving_registered_masked_path, threshold=1)
		# register_anima_non_linear(fixed_masked_path, moving_registered_masked_path, moving_registered_nl_path, transform_nl_path)
		

		# utils.apply_transforms_sitk(fixed_path, moving_path, [transform_path, transform_nl_path], moving_registered_nl_path)

		# # fixed_masked = sitk.ReadImage(str(fixed_masked_path), sitk.sitkFloat64)
		# # moving_registered_masked = sitk.ReadImage(str(moving_registered_masked_path), sitk.sitkFloat64)
		
		# # transform_nl = bspline_intra_modal_registration(fixed_masked, moving_registered_masked)
		# # moving_registered_nl = utils.apply_transform(fixed_masked, moving_registered_masked, transform_nl)
		
		# # sitk.WriteImage(moving_registered_nl, str(moving_registered_nl_path))

		# recompose the entire image
		if best_registration['patch_similarity'] > -6:
			continue

		registered_patch = moving_patch_registered 
		moving_registered_nl = sitk.ReadImage(str(moving_registered_nl_path), sitk.sitkFloat64)
		moving_registered = sitk.Cast(best_registration['moving_registered'], sitk.sitkFloat64)

		source_index = [0, 0, 0]
		registered_patch_size = list(registered_patch.GetSize())
		source_index[axis] = int(0.25 * registered_patch_size[axis]) if i>0 else 0
		source_size = list(registered_patch_size)
		source_size[axis] = int((0.5 if i>0 and i<len(fixed_patches)-1 else 0.75) * source_size[axis])
		
		index = registered.TransformPhysicalPointToIndex(registered_patch.TransformIndexToPhysicalPoint(source_index))
		pif.SetSourceIndex(index)
		pif.SetSourceSize(source_size)
		pif.SetDestinationIndex(index)
		
		
		registered = pif.Execute(registered, moving_registered)
		registered_nl = pif.Execute(registered_nl, moving_registered_nl)
		# sitk.WriteImage(registered, str(utils.configuration['output_folder'] / f'registered_{i}.nii.gz'))
		# sitk.WriteImage(registered_nl, str(utils.configuration['output_folder'] / f'registered_nl_{i}.nii.gz'))
	
	sitk.WriteImage(registered, str(utils.configuration['output_folder'] / f'registered.nii.gz'))
	sitk.WriteImage(registered_nl, str(utils.configuration['output_folder'] / f'registered_nl.nii.gz'))

	return registrations


if __name__ == '__main__':

	parser = argparse.ArgumentParser(
		prog=__file__,
		formatter_class=argparse.ArgumentDefaultsHelpFormatter,
		description="""Register rigidly and non-linearly two spinal cord images from a given patient. The method is designed to be robust to difference of sagital coverage and to partial overlap in the two images. However, images must have similar intensity characteristics (typically same sequence).""")

	parser.add_argument('-f', '--fixed', required=True, help='Path to the reference image.')
	parser.add_argument('-m', '--moving', required=True, help='Path to the image to register.')
	parser.add_argument('-ps', '--patch_size', type=float, default=100, help='Patch size.')
	parser.add_argument('-ov', '--overlap', type=float, default=0.75, help='Patch overlap.')
	parser.add_argument('-o', '--output_folder', default=None, help='Path to the output folder. Default is the current directory.')

	utils.add_args(parser)

	args = parser.parse_args()

	utils.load_config(args.configuration)

	output_folder = Path(args.output_folder) if args.output_folder else Path()
	output_folder.mkdir(exist_ok=True, parents=True)

	utils.set_debug(output_folder, args)

	fixed_path = Path(args.fixed)
	moving_path = Path(args.moving)
	
	fixed = sitk.ReadImage(str(fixed_path))
	moving = sitk.ReadImage(str(moving_path))

	register_patches(fixed, moving, similarity_early_stop_threshold='disabled', patch_size_mm=args.patch_size, patch_overlap=args.overlap)
4
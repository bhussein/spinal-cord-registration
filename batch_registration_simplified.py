import os
import subprocess
import argparse
import utils
from pathlib import Path


parser = argparse.ArgumentParser(
    prog=__file__,
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    description="""Register rigidly and non-linearly two spinal cord images from a given patient. The method is designed to be robust to difference of sagital coverage and to partial overlap in the two images. However, images must have similar intensity characteristics (typically same sequence).""")

parser.add_argument('-f', '--fixed_folder', required=True, help='Path to the reference image.')
parser.add_argument('-m', '--moving_folder', required=True, help='Path to the image to register.')
parser.add_argument('-o', '--output_folder', default=None, help='Path to the output folder. Default is the current directory.')

utils.add_args(parser)

args = parser.parse_args()

fu_dir = Path(args.fixed_folder)
bl_dir = Path(args.moving_folder)
output_dir = Path(args.output_folder)

def batch_registration(fu_dir, bl_dir, output_dir):

    subjects = os.listdir(fu_dir)
    
    for subj in subjects:

        subj_name = subj.split('.')[0]
        output = os.path.join(output_dir,subj_name)
        os.makedirs(output, exist_ok=True)
        print("[INFO] Processing subject: ", subj)
        
        subprocess.call(["python", "register.py", "-f",os.path.join(fu_dir, subj), "-m", os.path.join(bl_dir, subj), "-o", output])


batch_registration(fu_dir, bl_dir, output_dir)

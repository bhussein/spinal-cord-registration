# import utils
import SimpleITK as sitk
import csv
# import itk

# import registration_utilities as ru
# import registration_callbacks as rc

# fixed_image = sitk.ReadImage('/data/amasson/test/scr30_5/fixed.nii.gz', sitk.sitkFloat64)
# moving_image = sitk.ReadImage('/data/amasson/test/scr30_5/moving_on_fixed_rigid.nii.gz', sitk.sitkFloat64)

# fixed_image = itk.imread('/data/amasson/test/scr30_5/fixed.nii.gz')
# moving_image = itk.imread('/data/amasson/test/scr30_5/moving_on_fixed_rigid.nii.gz')

matches = []

# with open('/data/amasson/test/scr30_5/moving_on_fixed_rigid_matches.csv', newline='') as f:
with open('/data/amasson/test/scr30_5/matches2.csv', newline='') as f:
	matches = list(csv.reader(f))

fixed_points = []
moving_points = []

for match in matches:
	moving_points.append([int(float(m)) for m in match[3:]])
	fixed_points.append([int(float(m)) for m in match[:3]])

for points, name in [(fixed_points, 'fixed'), (moving_points, 'moving')]:
	with open(f'/data/amasson/test/scr30_5/{name}_points.pts', 'w') as f:
		f.write('index')
		f.write(str(len(points)))
		for point in points:
			f.write(f'{point[0]} {point[1]} {point[2]}')


# matches are 2 voxels (= 6 coordinates): moving_point,fixed_point

# for match in matches:
# 	moving_points.append(moving_image.TransformIndexToPhysicalPoint([int(float(m)) for m in match[3:]]))
# 	fixed_points.append(fixed_image.TransformIndexToPhysicalPoint([int(float(m)) for m in match[:3]]))

# def bspline_intra_modal_registration(
# 	fixed_image,
# 	moving_image,
# 	fixed_image_mask=None,
# 	fixed_points=None,
# 	moving_points=None,
# ):

# 	registration_method = sitk.ImageRegistrationMethod()

# 	# Determine the number of BSpline control points using the physical spacing we want for the control grid.
# 	grid_physical_spacing = [50.0, 50.0, 50.0]  # A control point every 50mm
# 	image_physical_size = [
# 		size * spacing
# 		for size, spacing in zip(fixed_image.GetSize(), fixed_image.GetSpacing())
# 	]
# 	mesh_size = [
# 		int(image_size / grid_spacing + 0.5)
# 		for image_size, grid_spacing in zip(image_physical_size, grid_physical_spacing)
# 	]

# 	initial_transform = sitk.BSplineTransformInitializer(
# 		image1=fixed_image, transformDomainMeshSize=mesh_size, order=3
# 	)
# 	registration_method.SetInitialTransform(initial_transform)

# 	registration_method.SetMetricAsMeanSquares()
# 	# Settings for metric sampling, usage of a mask is optional. When given a mask the sample points will be
# 	# generated inside that region. Also, this implicitly speeds things up as the mask is smaller than the
# 	# whole image.
# 	registration_method.SetMetricSamplingStrategy(registration_method.RANDOM)
# 	registration_method.SetMetricSamplingPercentage(0.01)
# 	if fixed_image_mask:
# 		registration_method.SetMetricFixedMask(fixed_image_mask)

# 	# Multi-resolution framework.
# 	registration_method.SetShrinkFactorsPerLevel(shrinkFactors=[4, 2, 1])
# 	registration_method.SetSmoothingSigmasPerLevel(smoothingSigmas=[2, 1, 0])
# 	registration_method.SmoothingSigmasAreSpecifiedInPhysicalUnitsOn()

# 	registration_method.SetInterpolator(sitk.sitkLinear)
# 	registration_method.SetOptimizerAsLBFGSB(
# 		gradientConvergenceTolerance=1e-5, numberOfIterations=100
# 	)

# 	# If corresponding points in the fixed and moving image are given then we display the similarity metric
# 	# and the TRE during the registration.
# 	if fixed_points and moving_points:
# 		registration_method.AddCommand(
# 			sitk.sitkStartEvent, rc.metric_and_reference_start_plot
# 		)
# 		registration_method.AddCommand(
# 			sitk.sitkEndEvent, rc.metric_and_reference_end_plot
# 		)
# 		registration_method.AddCommand(
# 			sitk.sitkIterationEvent,
# 			lambda: rc.metric_and_reference_plot_values(
# 				registration_method, fixed_points, moving_points
# 			),
# 		)

# 	return registration_method.Execute(fixed_image, moving_image)


# def demons_registration(
# 	fixed_image, moving_image, fixed_points=None, moving_points=None
# ):

# 	registration_method = sitk.ImageRegistrationMethod()

# 	# Create initial identity transformation.
# 	transform_to_displacment_field_filter = sitk.TransformToDisplacementFieldFilter()
# 	transform_to_displacment_field_filter.SetReferenceImage(fixed_image)
# 	# The image returned from the initial_transform_filter is transferred to the transform and cleared out.
# 	initial_transform = sitk.DisplacementFieldTransform(
# 		transform_to_displacment_field_filter.Execute(sitk.Transform())
# 	)

# 	# Regularization (update field - viscous, total field - elastic).
# 	initial_transform.SetSmoothingGaussianOnUpdate(
# 		varianceForUpdateField=0.0, varianceForTotalField=2.0
# 	)

# 	registration_method.SetInitialTransform(initial_transform)

# 	registration_method.SetMetricAsDemons(
# 		10
# 	)  # intensities are equal if the difference is less than 10HU

# 	# Multi-resolution framework.
# 	registration_method.SetShrinkFactorsPerLevel(shrinkFactors=[4, 2, 1])
# 	registration_method.SetSmoothingSigmasPerLevel(smoothingSigmas=[8, 4, 0])

# 	registration_method.SetInterpolator(sitk.sitkLinear)
# 	# If you have time, run this code as is, otherwise switch to the gradient descent optimizer
# 	# registration_method.SetOptimizerAsConjugateGradientLineSearch(learningRate=1.0, numberOfIterations=20, convergenceMinimumValue=1e-6, convergenceWindowSize=10)
# 	registration_method.SetOptimizerAsGradientDescent(
# 		learningRate=1.0,
# 		numberOfIterations=20,
# 		convergenceMinimumValue=1e-6,
# 		convergenceWindowSize=10,
# 	)
# 	registration_method.SetOptimizerScalesFromPhysicalShift()

# 	# If corresponding points in the fixed and moving image are given then we display the similarity metric
# 	# and the TRE during the registration.
# 	if fixed_points and moving_points:
# 		registration_method.AddCommand(
# 			sitk.sitkStartEvent, rc.metric_and_reference_start_plot
# 		)
# 		registration_method.AddCommand(
# 			sitk.sitkEndEvent, rc.metric_and_reference_end_plot
# 		)
# 		registration_method.AddCommand(
# 			sitk.sitkIterationEvent,
# 			lambda: rc.metric_and_reference_plot_values(
# 				registration_method, fixed_points, moving_points
# 			),
# 		)

# 	return registration_method.Execute(fixed_image, moving_image)

# print('register bspline')
# tx = bspline_intra_modal_registration(
# 	fixed_image=fixed_image,
# 	moving_image=moving_image,
# 	fixed_points=fixed_points,
# 	moving_points=moving_points,
# )

# moving_transformed = utils.apply_transform(fixed_image, moving_image, tx)
# sitk.WriteImage(moving_transformed, '/data/amasson/test/scr30_5/moving_bspline2.nii.gz')
# print('register demons')
# tx = demons_registration(
# 	fixed_image=fixed_image,
# 	moving_image=moving_image,
# 	fixed_points=fixed_points,
# 	moving_points=moving_points,
# )

# moving_transformed = utils.apply_transform(fixed_image, moving_image, tx)
# sitk.WriteImage(moving_transformed, '/data/amasson/test/scr30_5/moving_demons2.nii.gz')


Dimension = 3
# PixelType = itk.ctype("unsigned char")
# ImageType = itk.Image[PixelType, Dimension]


# def create_fixed_image():
#     start = [
#         0,
#     ] * Dimension
#     size = [
#         100,
#     ] * Dimension

#     region = itk.ImageRegion[Dimension](start, size)

#     ImageType = itk.Image[PixelType, Dimension]
#     image = ImageType.New()
#     image.SetRegions(region)
#     image.Allocate()
#     image.FillBuffer(0)

#     image[11:20, 11:20] = 255

#     itk.imwrite(image, "fixedPython.png")

#     return image


# def create_moving_image():
#     start = [
#         0,
#     ] * Dimension
#     size = [
#         100,
#     ] * Dimension

#     region = itk.ImageRegion[Dimension](start, size)

#     ImageType = itk.Image[PixelType, Dimension]
#     image = ImageType.New()
#     image.SetRegions(region)
#     image.Allocate()
#     image.FillBuffer(0)

#     image[51:60, 51:60] = 100

#     itk.imwrite(image, "movingPython.png")

#     return image


# fixed_image = create_fixed_image()
# moving_image = create_moving_image()

# LandmarkPointType = itk.Point[itk.D, Dimension]
# LandmarkContainerType = itk.vector[LandmarkPointType]

# fixed_landmarks = LandmarkContainerType()
# moving_landmarks = LandmarkContainerType()

# fixed_point = LandmarkPointType()
# moving_point = LandmarkPointType()

# for point in fixed_points:
# 	fixed_landmarks.push_back(point)
# for point in moving_points:
# 	moving_landmarks.push_back(point)

# TransformInitializerType = itk.LandmarkBasedTransformInitializer[
#     itk.Transform[itk.D, Dimension, Dimension]
# ]
# transform_initializer = TransformInitializerType.New()

# transform_initializer.SetFixedLandmarks(fixed_landmarks)
# transform_initializer.SetMovingLandmarks(moving_landmarks)

# transform = itk.BSplineTransform[itk.D, 3, 3].New()


# fixedPhysicalDimensions = []
# meshSize = MeshSizeType();
# for (unsigned int i = 0; i < ImageDimension; ++i)
# {
# fixedPhysicalDimensions[i] =
# 	fixedImage->GetSpacing()[i] * static_cast<double>(fixedImage->GetLargestPossibleRegion().GetSize()[i] - 1);
# }
# unsigned int numberOfGridNodesInOneDimension = 5;
# meshSize.Fill(numberOfGridNodesInOneDimension - SplineOrder);
# transform->SetTransformDomainOrigin(fixedImage->GetOrigin());
# transform->SetTransformDomainPhysicalDimensions(fixedPhysicalDimensions);
# transform->SetTransformDomainMeshSize(meshSize);
# transform->SetTransformDomainDirection(fixedImage->GetDirection());

# using ParametersType = TransformType::ParametersType;

# const unsigned int numberOfParameters = transform->GetNumberOfParameters();

# ParametersType parameters(numberOfParameters);

# parameters.Fill(0.0);

# transform->SetParameters(parameters);

# transform_initializer.SetTransform(transform)
# transform_initializer.InitializeTransform()

# output = itk.resample_image_filter(
#     moving_image,
#     transform=transform,
#     use_reference_image=True,
#     reference_image=fixed_image,
#     default_pixel_value=0,
# )

# itk.imwrite(output, '/data/amasson/test/scr30_5/moving_on_fixed_nl_itk.nii.gz')


# resultImage = sitk.Elastix(sitk.ReadImage('/data/amasson/test/scr30_5/fixed.nii.gz'), sitk.ReadImage('/data/amasson/test/scr30_5/moving_on_fixed_rigid.nii.gz'))
# sitk.WriteImage(resultImage, '/data/amasson/test/scr30_5/moving_on_fixed_elastix2.nii.gz')

fixedImage = sitk.ReadImage("/data/amasson/test/scr30_5/fixed.nii.gz")
movingImage = sitk.ReadImage("/data/amasson/test/scr30_5/moving.nii.gz")

# Compute the transformation
elastixImageFilter = sitk.ElastixImageFilter()
# elastixImageFilter.AddParameter( "Metric", "CorrespondingPointsEuclideanDistanceMetric" )
# elastixImageFilter.AddParameter( 2, "Metric", "CorrespondingPointsEuclideanDistanceMetric" )
# elastixImageFilter.AddParameter( 2, "Metric", "CorrespondingPointsEuclideanDistanceMetric" )
# elastixImageFilter.SetParameter( 2, "Metric2Weight", "1")
elastixImageFilter.SetParameter( 2, "Transform", "RecursiveBSplineTransform")
elastixImageFilter.AddParameter( 2, "Metric", "AnySimilarityMetric" "TransformBendingEnergyPenalty")
elastixImageFilter.SetParameter( 2, "Metric2Weight", "0.5")

elastixImageFilter.SetFixedImage(fixedImage)
elastixImageFilter.SetMovingImage(movingImage)


elastixImageFilter.SetFixedPointSetFileName('/data/amasson/test/scr30_5/fixed_points.pts')
elastixImageFilter.SetMovingPointSetFileName('/data/amasson/test/scr30_5/moving_points.pts')
elastixImageFilter.Execute()


sitk.WriteImage(elastixImageFilter.GetResultImage(), '/data/amasson/test/scr30_5/moving_on_fixed_elastix_points2.nii.gz')



# parameterMap = sitk.GetDefaultParameterMap("bspline")
# parameterMap["Metric"].append("CorrespondingPointsEuclideanDistanceMetric")

# elastixImageFilter = sitk.ElastixImageFilter()
# elastixImageFilter.AddParameter( "Metric", "CorrespondingPointsEuclideanDistanceMetric" )
